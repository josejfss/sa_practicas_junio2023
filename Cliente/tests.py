import unittest
from app import app
import requests
from flask import Flask, request, jsonify
import datetime


class FlaskTestCase(unittest.TestCase):
    
    def setUp(self):
        app.testing = True
        self.app = app.test_client()

    def test_verificar_orden_repartidor_c(self):
        # Realizar solicitud HTTP POST a la ruta
        resultado = requests.post(
        'http://localhost:4000/cliente/verificar_orden_repartidor', 
        data = {
            'orden_id': 'e06a2fe2-85e3-465c-aa96-55f2014fc479'
        } 
        )
        # Verificar que la respuesta sea correcta
        self.assertEqual(resultado.status_code, 200)

    
    def test_verificar_orden_restaurante_c(self):
        # Realizar solicitud HTTP POST a la ruta
        resultado = requests.post(
        'http://localhost:4000/cliente/verificar_orden_restaurante', 
        data = {
            'orden_id': 'e06a2fe2-85e3-465c-aa96-55f2014fc479'
        } 
        )
        # Verificar que la respuesta sea correcta
        self.assertEqual(resultado.status_code, 200)


    def test_solicitar_pedido_c(self):
        # Realizar solicitud HTTP POST a la ruta
        pedido = "Esto es un pedido de prueba unitaria"
        direccion = "Esto es una direccion de prueba unitaria"
        now = datetime.datetime.now()
        fecha_tiempo = now.strftime("%Y-%m-%d %H:%M:%S")

        resultado = requests.post(
        'http://localhost:4000/cliente/solicitar_pedido_r', 
        data = {
            'log':  fecha_tiempo + ' - cliente/solicitar_pedido: ' + pedido + '|' + direccion,
            'pedido': pedido,
            'direccion': direccion
        } 
        )
        print(resultado)

        # Verificar que la respuesta sea correcta
        self.assertEqual(resultado.status_code, 200)

if __name__ == '__main__':
    unittest.main()
