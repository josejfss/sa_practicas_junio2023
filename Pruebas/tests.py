import unittest
from app import app

class FlaskTestCase(unittest.TestCase):
    
    def setUp(self):
        app.testing = True
        self.app = app.test_client()

    def test_saludo(self):
        nombre = 'Juan'
        resultado_esperado = 'Hola, Juan'

        # Realizar solicitud HTTP GET a la ruta '/saludo/Juan'
        resultado = self.app.get(f'/saludo/{nombre}')
        print(resultado)

        # Verificar que la respuesta sea correcta
        self.assertEqual(resultado.data.decode('utf-8'), resultado_esperado)

if __name__ == '__main__':
    unittest.main()
